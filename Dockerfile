FROM odoo:13.0

USER root

RUN apt-get update \
    && apt-get install -y build-essential libssl-dev libffi-dev python3-dev

COPY requirements.txt /tmp/

RUN pip3 install --upgrade pip setuptools

RUN pip3 install --no-cache-dir -r /tmp/requirements.txt