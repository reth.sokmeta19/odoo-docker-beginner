# Odoo Docker Set up for beginner

## Prerequisite

Before proceeding, make sure you have the following software installed on your system:

* `Docker`: for create and manage Containers. For installation instructions [official Docker Installation](https://docs.docker.com/get-docker)
* `Docker Compose`: for defining and running multi-container applications. For installation instructions [official Docker Compose installation](https://docs.docker.com/compose/install)

## Set up

After cloning this repository, you have to follow this steps:

### cd into directory
```
cd odoo-docker-beginner
```

### Copy env
```
cp .env.sample .env
```
You can add or modify env as you need

### odoo.conf
It is placed inside `/config/odoo.conf`
You can add or modify conf as you need

### Build docker
```
docker-compose build
```

### Place necessary addons into folder
- `extra-addons`: for custom addons
- `third-party-addons`: for Third Party Addons

### Run docker
```
docker-compose up -d
```

## Docker command
### Stop both Odoo and Postgres
```
docker-compose down
```


### Stop single container 
```
docker-compose down <container_name>
```
- container_name can be `odoo` or `db`

### Restart container
```
docker-compose restart
```
or
```
docker-compose restart odoo
```